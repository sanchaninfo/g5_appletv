/***
 **Module Name:  PlayerLayerView Controller.
 **File Name :  PlayerLayerView Controller.swift
 **Project :   GFive Cultivation
 **Copyright(c) : GFive Cultivation.
 **Organization : Peafowl Inc
 **author :  Vijay Bhaskar
 **author :  Manikumar
 **license :
 **version :  1.0.0
 **Created on :
 **Last modified on:
 **Description : Player.
 */

import UIKit
import AVKit
import AVFoundation


protocol playerdelegate:class {
    func getassetdata(withUrl:String,id:String,userid:String)
}


class PlayerLayerViewController: UIViewController,playerEndDelegate,UICollectionViewDataSource,UICollectionViewDelegate {
    
    @IBOutlet var ProductView: UIView!
    @IBOutlet var TransparentView: UIView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var topView: UIView!
   
    @IBOutlet weak var ProductPrice: UILabel!
    @IBOutlet weak var buyBtn: UIButton!
    @IBOutlet weak var ProductTiltle: UILabel!
    @IBOutlet weak var ProductLogo: UIImageView!
    @IBOutlet weak var shopBtn: UIButton!
    @IBOutlet weak var closeBtn:UIButton!
    @IBOutlet var ourPriceLbl: UILabel!
    @IBOutlet weak var containerView: UIView!

    @IBOutlet weak var childView: UIView!
   
    var playerLayer:AVPlayerLayer!
    var playerItem:AVPlayerItem!
    var uuid = String()
    
    var videoUrl,userID,videoID,mainVideoID,deviceID : String!
    var seektime = Float64()
    var updatetime = Float64()
    var isResume = Bool()
    var detdelegate:playerdelegate?
    var resumeTime = Float64()
    var getnextAsset = Bool()
    var UpdateTimer = Timer()
    var isplayEnd = Bool()
    var nextAsset = NSDictionary()
    var isMyList = Bool()
    var nexttimer = Timer()
    var player:AVPlayer!
    var isMenuPressed = Bool()
    var timeObserver:Any?
    var notifyObserver:Any?
    var isdurPlay = Bool()
    let focusGuide = UIFocusGuide()
    var avplayerController = AVPlayerViewController()
    var issearch = Bool()
    var storeData = [[String:Any]]()
    var productCollectionList = NSMutableArray()
    var prod = NSDictionary()
    var queuePlayer:AVQueuePlayer!
    var videoItemList = [AVPlayerItem]()
    var videoItem:AVPlayerItem!
    var activityView = UIView()
    let topButtonFocusGuide = UIFocusGuide()
    var isPlayerpaused = Bool()
    var startAtInterval = Float64()
    var stopAtInterval = Float64()
    var isProductVisible = Bool()
    var DonateData = [[String:Any]]()
    var isDonate = Bool()
    var monitizeLbl = String()
    var monitizetype = String()
    var buyDict = NSDictionary()
    var isfromplayend = Bool()
    var accountDict = NSDictionary()
    var carouselName = String()
   
  
    
    var viewToFocus: UIView? = nil {
        didSet {
            if viewToFocus != nil {
                self.setNeedsFocusUpdate();
                self.updateFocusIfNeeded();
            }
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
      
        getaccountInfo()
      //  activityView = ActivityView.init(frame: self.view.frame)
      //  self.view.addSubview(activityView)
        self.view.bringSubview(toFront: childView)
        getproducts()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleMenuPress))
        tapGesture.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue)]
        self.view.addGestureRecognizer(tapGesture)
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(handlePausePress))
        tapGesture1.allowedPressTypes = [NSNumber(value: UIPressType.playPause.rawValue)]
        self.view.addGestureRecognizer(tapGesture1)
        collectionView.isHidden = true
        setUpPlayerVideos()
        
        buyBtn.layer.cornerRadius = 7.0
        ProductView.layer.cornerRadius = 7.0
        shopBtn.layer.cornerRadius = min(shopBtn.frame.size.height, shopBtn.frame.size.width)/2.0
        shopBtn.clipsToBounds = true
        closeBtn.layer.cornerRadius = min(closeBtn.frame.size.height,closeBtn.frame.size.width)/2.0
        closeBtn.clipsToBounds = true
        
        if #available(tvOS 10.0, *) {
            topButtonFocusGuide.preferredFocusEnvironments = [buyBtn]
        } else {
            // Fallback on earlier versions
        }
        self.view.addLayoutGuide(topButtonFocusGuide)
        self.view.addConstraints([topButtonFocusGuide.topAnchor.constraint(equalTo: TransparentView.topAnchor), topButtonFocusGuide.bottomAnchor.constraint(equalTo: TransparentView.bottomAnchor), topButtonFocusGuide.leadingAnchor.constraint(equalTo: TransparentView.leadingAnchor), topButtonFocusGuide.widthAnchor.constraint(equalTo: TransparentView.widthAnchor)])
        collectionView.isHidden = true
        ProductView.isHidden = true
        shopBtn.isHidden = true
        closeBtn.isHidden = true
        
        //  avplayerController.view.frame = self.view.frame
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        /*    if isfromplayend
         {
         activityView = ActivityView.init(frame: self.view.frame)
         self.view.addSubview(activityView)
         getproducts()
         //            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleMenuPress))
         //            tapGesture.allowedPressTypes = [NSNumber(value: UIPressType.menu.rawValue)]
         //            self.view.addGestureRecognizer(tapGesture)
         //            let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(handlePausePress))
         //            tapGesture1.allowedPressTypes = [NSNumber(value: UIPressType.playPause.rawValue)]
         //            self.view.addGestureRecognizer(tapGesture1)
         //            collectionView.isHidden = true
         setUpPlayerVideos()
         
         //            buyBtn.layer.cornerRadius = 7.0
         //            ProductView.layer.cornerRadius = 7.0
         //            shopBtn.layer.cornerRadius = min(shopBtn.frame.size.height, shopBtn.frame.size.width)/2.0
         //            shopBtn.clipsToBounds = true
         //            closeBtn.layer.cornerRadius = min(closeBtn.frame.size.height,closeBtn.frame.size.width)/2.0
         //            closeBtn.clipsToBounds = true
         //
         //            if #available(tvOS 10.0, *) {
         //                topButtonFocusGuide.preferredFocusEnvironments = [buyBtn]
         //            } else {
         //                // Fallback on earlier versions
         //            }
         //            self.view.addLayoutGuide(topButtonFocusGuide)
         //            self.view.addConstraints([topButtonFocusGuide.topAnchor.constraint(equalTo: TransparentView.topAnchor), topButtonFocusGuide.bottomAnchor.constraint(equalTo: TransparentView.bottomAnchor), topButtonFocusGuide.leadingAnchor.constraint(equalTo: TransparentView.leadingAnchor), topButtonFocusGuide.widthAnchor.constraint(equalTo: TransparentView.widthAnchor)])
         //            collectionView.isHidden = true
         //            ProductView.isHidden = true
         //            shopBtn.isHidden = true
         //            closeBtn.isHidden = true
         
         }*/
        //  else
        //   {
       // self.queuePlayer.play()
        self.player.play()
        getnextAsset = false
        isdurPlay = false
        //    }
        
    }
    
    func getproducts()
    {
        if isDonate
        {
            prod = (DonateData.first! as NSDictionary)
            self.ourPriceLbl.text = "Donation:"
        }
        else
        {
            prod = (storeData.first! as NSDictionary)
            self.ourPriceLbl.text = "our Price:"
        }
        let individualcarousel = prod["carousels"] as! NSArray
        let carousalid = individualcarousel.firstObject as! NSDictionary
        let startAt = carousalid["startAt"] as! String
        let stopAt = carousalid["stopAt"] as! String
        
        let startT = startAt.components(separatedBy: ":")
        let starthour = (Int(startT[0]))! * 60 * 60
        let startminute = Int(startT[1])! * 60
        let startseconds = Int(startT[2])!
        startAtInterval = Float64(starthour + startminute + startseconds)
        
        let stopT = stopAt.components(separatedBy: ":")
        let stophour = (Int(stopT[0]))! * 60 * 60
        let stopminutes = Int(stopT[1])! * 60
        let stopseconds = Int(stopT[2])!
        stopAtInterval = Float64(stophour + stopminutes + stopseconds)
        let productList = carousalid["products"] as! NSArray
        for individualProduct in productList
        {
            let dict = individualProduct as! NSDictionary
            productCollectionList.add(dict)
        }
        //        if isDonate
        //        {
        //            self.shopBtn.setTitle("DONATE", for: .normal)
        //        }
        //        else
        //        {
        //           self.shopBtn.setTitle("SHOP THE COLLECTION(\(productCollectionList.count))", for: .normal)
        //        }
        let lbl = monitizeLbl.uppercased()
        self.shopBtn.setTitle("\(lbl)", for: .normal)
    }
    func handlePausePress()
    {
        if (isPlayerpaused == false)
        {
          //  queuePlayer.pause()
            player.pause()
            isPlayerpaused = true
        }
        else
        {
           // queuePlayer.play()
            player.play()
            isPlayerpaused = false
        }
    }
    
    
    func handleMenuPress()
    {
        self.isMenuPressed = true
        
        UpdateTimer.invalidate()
        player.pause()
     //   queuePlayer.pause()
        player.removeTimeObserver(timeObserver as Any)
      //  queuePlayer.removeTimeObserver(timeObserver as Any)
      //  queuePlayer = nil
        player = nil
        self.detdelegate?.getassetdata(withUrl:kAssestDataUrl,id:self.videoID,userid:self.userID)
        if !issearch
        {
            if isfromplayend
            {
                for viewcontroller in (self.navigationController?.viewControllers)!
                {
                    if viewcontroller.isKind(of: DetailPageViewController.self)
                    {
                        let  _ =  self.navigationController?.popToViewController(viewcontroller, animated: false)
                    }
                }
            }
            else
            {
                
                let _ = self.navigationController?.popViewController(animated: true)
            }
        }
        else
        {
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productCollectionList.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
        let path = productCollectionList[indexPath.row] as! NSDictionary
        let carousalImage = path["images"] as! NSArray
        let imageThumb = carousalImage.firstObject as! NSDictionary
        let imageUrl = kStoreBaseUrl + (imageThumb["thumb"] as! String)
        let img = (cell.viewWithTag(10) as! UIImageView)
        img.kf.indicatorType = .activity
        (cell.viewWithTag(10) as! UIImageView).kf.setImage(with: URL(string: imageUrl))
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if let prev = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: prev)
        {
            
            (cell.viewWithTag(10) as! UIImageView).transform = .identity
            
        }
        if let next = context.nextFocusedIndexPath,
            let cell = collectionView.cellForItem(at: next)
        {
            
            
            let individualcarousel = prod.object(forKey: "carousels") as! NSArray
            let carousalDict = individualcarousel.firstObject as! NSDictionary
            
            let logo = carousalDict["carouselLogo"] as! String
            self.ProductLogo.kf.setImage(with: URL(string: logo))
            let path = productCollectionList[next.row] as! NSDictionary
            self.buyDict = path
            
            self.buyBtn.setTitle(monitizeLbl, for: .normal)
            //  ProductTiltle.text = path["title"] as? String
            ProductPrice.text = "$" + "\(path["price"] as! String)"
            (cell.viewWithTag(10) as! UIImageView).adjustsImageWhenAncestorFocused = true
            //  collectionIndex = next.row
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let Path = productCollectionList[indexPath.row] as! NSDictionary
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        self.queuePlayer.pause()
        if monitizetype == "Fixed Donation"
        {
            
            let contribute = storyBoard.instantiateViewController(withIdentifier: "Contribute") as! ContributeViewController
            contribute.donationDict = Path
            contribute.accountDict = self.accountDict
            if accountDict.count != 0
            {
                self.navigationController?.pushViewController(contribute, animated: true)
            }
            else
            {
                getaccountInfo()
            }
            
        }
        else
        {
            let productdetailpage = storyBoard.instantiateViewController(withIdentifier: "productdetails") as! StoreProductDetailsViewController
            productdetailpage.productlist = Path
            productdetailpage.userId = userID
            productdetailpage.uuid = uuid
            productdetailpage.deviceId = deviceID
            productdetailpage.fromPlayLayer = true
            self.navigationController?.pushViewController(productdetailpage, animated: true)
        }
        
    }
    
    // playvideo Implementation
    func playVideo(userId:String,videoId:String,deviceId:String,MyList:Bool) {
        
        userID = userId
        videoID = videoId
        deviceID = deviceId
        isMyList = MyList
        if isResume
        {
            
            let targetTime = CMTime(seconds: resumeTime, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
            player.seek(to: targetTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
         //   queuePlayer.seek(to: targetTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
            player.play()
        //    queuePlayer.play()
        }
        else
        {
            
         //   queuePlayer.play()
            player.play()
        }
        timeObserver = (player.addPeriodicTimeObserver(forInterval: CMTimeMake(1, 1), queue: DispatchQueue.main, using: {
            _ in
            if self.player.currentItem?.status == .readyToPlay
            {
             
                //self.activityView.removeFromSuperview()
                 self.seektime = CMTimeGetSeconds((self.player.currentItem?.currentTime())!)
                 let duration = CMTimeGetSeconds((self.player?.currentItem?.duration)!)
                if ((duration - self.seektime) <= 30.0) && (self.getnextAsset == false)
                {
                    if self.issearch == false
                    {
                        self.nextAssetData()
                        self.isMenuPressed = true
                    }
                }
                if (self.seektime >= (duration - 1.0)) && (self.isdurPlay == false)
                {
                    self.isdurPlay = true
                    if self.issearch == false
                    {
                        self.playerEnd()
                    }
                }
                
                let currentT = Int(self.seektime)
                if (currentT >= Int(self.startAtInterval)) && (self.isProductVisible == false) && (currentT <= Int(self.stopAtInterval)) //&& (self.isUseractive == false)
                {
                    if self.monitizeLbl != ""
                    {
                        self.view.bringSubview(toFront: self.containerView)
                        self.shopBtn.isHidden = false
                        self.closeBtn.isHidden = false
                        //   self.playeractiveBtn.isHidden = true
                        self.viewToFocus = self.shopBtn
                        self.isProductVisible = true
                        self.isMenuPressed = true
                        //  self.isUseractive = true
                    }
                }
                else if (currentT >= Int(self.stopAtInterval)) && (self.isProductVisible == true)// && (self.isUseractive == true)
                {
                    self.shopBtn.isHidden = true
                    self.closeBtn.isHidden = true
                    self.collectionView.isHidden = true
                    self.ProductView.isHidden = true
                }
                
                
            }
        }))
        
      /*  timeObserver = (queuePlayer?.addPeriodicTimeObserver(forInterval: CMTimeMake(1, 1), queue: DispatchQueue.main, using:
            {_ in
                if self.queuePlayer.currentItem?.status == .readyToPlay
                {
                    self.activityView.removeFromSuperview()
                    // self.collectionView.isHidden = false
                    self.seektime = CMTimeGetSeconds((self.queuePlayer.currentItem?.currentTime())!)
               
                    let duration = CMTimeGetSeconds((self.queuePlayer?.currentItem?.duration)!)
                    if ((duration - self.seektime) <= 30.0) && (self.getnextAsset == false)
                    {
                        if self.issearch == false
                        {
                            self.nextAssetData()
                            self.isMenuPressed = true
                        }
                    }
                    if (self.seektime >= (duration - 1.0)) && (self.isdurPlay == false)
                    {
                        self.isdurPlay = true
                        if self.issearch == false
                        {
                            self.playerEnd()
                        }
                    }
                    
                    let currentT = Int(self.seektime)
                    if (currentT >= Int(self.startAtInterval)) && (self.isProductVisible == false) && (currentT <= Int(self.stopAtInterval)) //&& (self.isUseractive == false)
                    {
                        if self.monitizeLbl != ""
                        {
                            self.shopBtn.isHidden = false
                            self.closeBtn.isHidden = false
                            //   self.playeractiveBtn.isHidden = true
                            self.viewToFocus = self.shopBtn
                            self.isProductVisible = true
                            self.isMenuPressed = true
                            //  self.isUseractive = true
                        }
                    }
                        
                    else if (currentT >= Int(self.stopAtInterval)) && (self.isProductVisible == true)// && (self.isUseractive == true)
                    {
                        self.shopBtn.isHidden = true
                        self.closeBtn.isHidden = true
                        self.collectionView.isHidden = true
                        self.ProductView.isHidden = true
                    }
                }
        }))*/
        
        UpdateTimer = Timer.scheduledTimer(timeInterval: 20, target: self, selector: #selector(self.UpdateseekTime), userInfo: nil, repeats: true)
        
    }
    
    @IBAction func shopAction(_ sender: AnyObject) {
        // Fixed Donation
        self.view.bringSubview(toFront: containerView)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let contribute = storyBoard.instantiateViewController(withIdentifier: "Contribute") as! ContributeViewController
        if monitizetype == "User Defined Donation"
        {
            shopBtn.isHidden = true
            closeBtn.isHidden = true
            collectionView.isHidden = true
            ProductView.isHidden = true
            contribute.accountDict = self.accountDict
            if self.accountDict.count != 0
            {
                self.navigationController?.pushViewController(contribute, animated: true)
            }
            else
            {
                getaccountInfo()
            }
            
            
        }
        else
        {
            shopBtn.isHidden = true
            closeBtn.isHidden = true
            collectionView.isHidden = false
            ProductView.isHidden = false
            viewToFocus = collectionView
        }
        
    }
    
    
    @IBAction func BuyBtn(_ sender: Any) {
        let Path = buyDict
        //        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        //        let detailpage = storyBoard.instantiateViewController(withIdentifier: "StoreDetail") as! StoreDetailViewController
        //        detailpage.productlist = Path
        //        detailpage.userId = userID
        //        detailpage.uuid = uuid
        //        detailpage.deviceId = deviceID
        //        detailpage.fromPlaylayer = true
        //        queuePlayer.pause()
        //        self.navigationController?.pushViewController(detailpage, animated: true)
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
       // self.queuePlayer.pause()
        self.player.pause()
        if monitizetype == "Fixed Donation"
        {
            
            let contribute = storyBoard.instantiateViewController(withIdentifier: "Contribute") as! ContributeViewController
            contribute.donationDict = Path
            contribute.accountDict = self.accountDict
            if accountDict.count != 0
            {
                self.navigationController?.pushViewController(contribute, animated: true)
            }
            else
            {
                getaccountInfo()
            }
            
        }
        else
        {
            let productdetailpage = storyBoard.instantiateViewController(withIdentifier: "productdetails") as! StoreProductDetailsViewController
            productdetailpage.productlist = Path
            productdetailpage.userId = userID
            productdetailpage.uuid = uuid
            productdetailpage.deviceId = deviceID
            productdetailpage.fromPlayLayer = true
            self.navigationController?.pushViewController(productdetailpage, animated: true)
        }
        
        
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        self.view.bringSubview(toFront: childView)
        shopBtn.isHidden = true
        closeBtn.isHidden = true
    }
    
    @IBAction func playerBtn(_ sender: Any) {
        if (isPlayerpaused == false)
        {
          //  queuePlayer.pause()
            player.pause()
            isPlayerpaused = true
        }
        else
        {
        //    queuePlayer.play()
            player.play()
            isPlayerpaused = false
        }
    }
    
    func playnextVideo()
    {
        let playerItem = AVPlayerItem(url: NSURL(string: videoUrl)! as URL)
        player = AVPlayer(playerItem: playerItem)
        player?.play()
    }
    
    // seektime
    func UpdateseekTime()
    {
        let parameters = ["updateSeekTime":["userId": userID as AnyObject, "videoId": (videoID) as AnyObject, "seekTime": self.seektime]]
        
        ApiManager.sharedManager.postDataWithJson(url: kUpdateseekUrl, parameters: parameters as [String : [String : AnyObject]]){(responseDict,error,isDone)in
            if error == nil
            {
                let JSON = responseDict as! NSDictionary
                if JSON["watchedVideo"] != nil
                {
                    if JSON["watchedVideo"] is NSNull
                    {
                        self.updatetime = 0.0
                    }
                    else
                    {
                        self.updatetime = Float64((((JSON)["watchedVideo"] as! NSDictionary)["seekTime"]) as! Float64)
                    }
                    UserDefaults.standard.set(self.updatetime, forKey: "seektime")
                    UserDefaults.standard.synchronize()
                    if self.updatetime > 0
                    {
                        // self.detdelegate?.getassetdata(withUrl:kAssestDataUrl,id:self.videoID,userid:self.userID)
                    }
                }
            }
            else
            {
                print("json error")
                //                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                //                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                //                    UIAlertAction in
                //                  let _ = self.navigationController?.popViewController(animated: true)
                //                })
                //                alertview.addAction(defaultAction)
                //                self.navigationController?.present(alertview, animated: true, completion: nil)
            }
        }
    }
    
    // Next Asset Data
    func nextAssetData()
    {
        print("Iam in next Asset Data")
        getnextAsset = true
        let parameters = ["getNextPlay": ["videoId": videoID, "userId": self.userID,"myList":self.isMyList,"deviceId":self.deviceID]]
        print(parameters)
        ApiManager.sharedManager.postDataWithJson(url:kNextAssetUrl ,parameters: parameters as [String : [String : AnyObject]])
        {
            (responseDict,error,isDone)in
            if error == nil
            {
                if responseDict is NSArray
                {
                    
                }
                else
                {
                    self.nextAsset = responseDict as! NSDictionary
                }
            }
            else
            {
                
            }
        }
        
    }
    // player end
    func playerEnd()
    {
        print("Iam in player end")
        seektime = 0.0
        self.isplayEnd = true
        UpdateseekTime()
        UpdateTimer.invalidate()
     //   getnextAsset = false
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let playerEnd = storyBoard.instantiateViewController(withIdentifier: "playerEnd") as! PlayerEndViewController
        if nextAsset.count != 0
        {
            playerEnd.userID = userID
            playerEnd.deviceID = deviceID
            playerEnd.getData(getnextData: self.nextAsset)
            playerEnd.playEnddelegate = self
            playerEnd.DonateData = self.DonateData
            playerEnd.storeData = self.storeData
          //  isdurPlay = false
            self.navigationController?.pushViewController(playerEnd, animated: true)
        }
        else
        {
            let _ = self.navigationController?.popViewController(animated: true)
        }
        
    }
    func playEnd(userId: String, videoId: String, deviceId: String, MyList: Bool,isfromplayend:Bool,storeData:[[String:Any]],DonateData:[[String:Any]])
    {
        self.userID = userId
        self.videoID = videoId
        self.deviceID = deviceId
        self.isMyList = MyList
        //  self.videoUrl = videoURL
        self.isfromplayend = isfromplayend
        self.storeData = storeData
        self.DonateData = DonateData
    }
    
    override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
        if context.nextFocusedView == buyBtn
        {
            //            buyBtn.layer.borderColor = focusColor
            //            buyBtn.layer.borderWidth = 5.0
            buyBtn.bounds = CGRect(x: buyBtn.bounds.origin.x, y: buyBtn.bounds.origin.y, width: buyBtn.bounds.size.width + 15, height: buyBtn.bounds.size.height + 15)
        }
        if context.nextFocusedView == shopBtn
        {
            //  shopBtn.backgroundColor = UIColor.init(red: 231/255, green: 0/255, blue: 37/255, alpha: 1)
            //   shopBtn.bounds = CGRect(x: shopBtn.bounds.origin.x, y: shopBtn.bounds.origin.y, width: shopBtn.bounds.size.width + 5, height: shopBtn.bounds.size.height + 5)
            shopBtn.layer.borderWidth = 4.0
            shopBtn.layer.borderColor = UIColor.white.cgColor
            
        }
        if context.nextFocusedView == closeBtn
        {
            //  closeBtn.bounds = CGRect(x: closeBtn.bounds.origin.x, y: closeBtn.bounds.origin.y, width: closeBtn.bounds.size.width + 5, height: closeBtn.bounds.size.height + 5)
            closeBtn.layer.borderWidth = 4.0
            closeBtn.layer.borderColor = UIColor.white.cgColor
        }
        if context.previouslyFocusedView == buyBtn
        {
            //   buyBtn.layer.borderWidth = 0.0
            buyBtn.bounds = CGRect(x: buyBtn.bounds.origin.x, y: buyBtn.bounds.origin.y, width: buyBtn.bounds.size.width - 15, height: buyBtn.bounds.size.height - 15)
        }
        if context.previouslyFocusedView == shopBtn
        {
            //  shopBtn.bounds = CGRect(x: shopBtn.bounds.origin.x, y: shopBtn.bounds.origin.y, width: shopBtn.bounds.size.width - 5, height: shopBtn.bounds.size.height - 5)
            shopBtn.layer.borderWidth = 0.0
            shopBtn.layer.borderColor = UIColor.clear.cgColor
        }
        if context.previouslyFocusedView == closeBtn
        {
            //  closeBtn.bounds = CGRect(x: closeBtn.bounds.origin.x, y: closeBtn.bounds.origin.y, width: closeBtn.bounds.size.width - 5, height: closeBtn.bounds.size.height - 5)
            closeBtn.layer.borderWidth = 0.0
            closeBtn.layer.borderColor = UIColor.clear.cgColor
        }
        //        if context.nextFocusedView == collectionView
        //        {
        //            viewToFocus = collectionView.cellForItem(at: IndexPath(row: collectionIndex, section: 0))
        //        }
    }
    
    
    func setUpPlayerVideos()
    {
        
        videoItemList.removeAll()
        //        for dict in urlList
        //        {
        playerItem = AVPlayerItem.init(url: URL(string: videoUrl)!)
        videoItemList.append(playerItem)
        //  }
        player = AVPlayer(playerItem: playerItem)
        player.replaceCurrentItem(with: playerItem)
        let av = AVPlayerViewController()
        av.player = player
       // playerLayer = AVPlayerLayer(player: player)
        av.view.frame = CGRect(x: 0, y: 0, width: 1920, height: 1080)
        self.addChildViewController(av)
        childView.addSubview(av.view)
    
        // queuePlayer = AVQueuePlayer(items: videoItemList)
      //  playerLayer = AVPlayerLayer.init(player: queuePlayer)
//        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
//        playerLayer.zPosition = -1
//        playerLayer.frame = CGRect(x: 0, y: 0, width: 1920, height: 1080)
//        topView.layer.addSublayer(playerLayer)
//        player.isMuted = false
      //  queuePlayer.isMuted = false
     
        playVideo(userId: self.userID, videoId: self.videoID, deviceId: self.deviceID, MyList: self.isMyList)
    }
    func getaccountInfo()
    {
        let url = kAccountInfoUrl
        let  parameters = [ "getAccountInfo": ["deviceId": deviceID!, "uuid": uuid]]
        ApiManager.sharedManager.postDataWithJson(url: url, parameters: parameters as [String : [String : AnyObject]]) {(responseDict , error,isDone) in
            if error == nil
            {
                let post = responseDict
                let dict = post as! NSDictionary
                self.accountDict = dict
            }
            else
            {
                
            }
        }
        
    }
}
